<?php
	$settings = simplexml_load_file('localhost/settings.xml');
	$hostname = ($settings->hostname == '' || $settings->hostname == null) ? 'http://localhost/' : $settings->hostname;
	$favourites = array();
	foreach($settings->favourites->fav as $fav){ $favourites[] = (string) $fav['inode']; }
	$images = array('png','jpg','jpg','gif');
	$request = rawurldecode(parse_url($_SERVER['REQUEST_URI'], PHP_URL_QUERY));
	$request .= (substr($request, -1) == '/') ? '' : '/';
	$request = (substr($request, 0) == '/') ? substr($request, 1) : $request;
?>
<!DOCTYPE html>
<html lang="en">
	<head>
  <meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0" />
		<title>localhost</title>
		<link href="/localhost/css/frontend.min.css" rel="stylesheet" type="text/css" />
		<script src="/localhost/js/jquery-1.10.2.min.js" type="text/javascript"></script>
		<script src="/localhost/js/frontend.js" type="text/javascript"></script>
	</head>
	<body>
		<section id="main-container">
			<aside>
				<header><h2><i class="icon-heart-outline"></i><span>Favourites</span></h2></header>
				<section class="content">
					<?php if(count($settings->favourites->fav) == 0): ?>
					<span id="no-favs">No favourites. You can add some quick-access files and folders to this list by clicking on its heart icon.</span>
					<?php else: ?>
					<ul>
						<?php foreach($settings->favourites->fav as $fav): ?>
						<li>
							<?php if($fav['type'] == 'dir'): ?>
								<a href="<?php echo $hostname ?>?<?php echo (string) $fav['path'] ?>" inode="<?php echo (string) $fav['inode'] ?>" path="<?php echo (string) $fav['path'] ?>"><?php echo (string) $fav['name'] ?></a>
							<?php else: ?>
							<a href="<?php echo (string) $fav['path'] ?>" inode="<?php echo (string) $fav['inode'] ?>" path="<?php echo (string) $fav['path'] ?>"><?php echo (string) $fav['name'] ?></a>
							<?php endif; ?>
						</li>
						<?php endforeach; ?>
					</ul>
					<? endif; ?>
				</section>
				<div id="jg-logo"><a href="http://joe.guari.no" title="Joe Guarino" id="jg-logo"><img src="localhost/jg-logo.png" alt="Joe Guarino" /></a></div>
			</aside>
			<section id="main">
				<header>
					<h2>
						<i class="icon-flow-merge"></i>
						<?php if(isRoot()): ?>
						<span><a href="<?php echo $hostname ?>" title="localhost" class="current">localhost</a></span>
						<?php else: ?>
						<span><a href="<?php echo $hostname ?>" title="localhost">localhost</a><?php echo breadcrumbs() ?></span>
						<?php endif; ?>
					</h2>
				</header>
				<section id="dir-content" class="content">
					<section class="action-items">
						<?php echo createNew() ?>
						<?php if(!isRoot()) echo previous() ?>
						<span class="clear"></span>
					</section>
					<section id="entries">
						<?php
							$cur_dir = getcwd();
							if(!isRoot()){
								$cur_dir .= '/' . $request;
								$cur_dir = $cur_dir;
							}
							$list = scan($cur_dir);
							if(count($list) == 0): ?>
								<span class="entry default">This folder is empty</span>
							<?php
							else:
								foreach($list as $file):
									if($file['isDir']):
									$path = $request . $file['filename']; ?>
										<a href="<?php echo $hostname ?>?<?php echo $path ?>/" class="entry dir" inode="<?php echo $file['inode'] ?>" path="<?php echo $path ?>" type="dir">
											<i class="icon-folder-close"></i><span><?php echo $file['filename'] ?></span>
									<?php else: $path = $request . $file['filename'] . '.' . $file['extension'] ?>
										<a href="<?php echo $path ?>" class="entry file" inode="<?php echo $file['inode'] ?>" path="<?php echo $path ?>" type="file">
										<i class="icon-document"></i><span><?php echo $file['filename'] . '.' . $file['extension'] ?></span>
									<?php endif; ?>
									<i class="fav <?php echo isFavved($file['inode']) ? 'favved' : '' ?>  icon-heart"></i></a>
								<?php
								endforeach;
							endif;
						?>
						</section>
						<section class="action-items">
							<?php echo createNew() ?>
							<?php if(!isRoot()) echo previous() ?>
							<span class="clear"></span>
						</section>
				</section>
			</section>
			<div class="clear"></div>
		</section>
	</body>
</html>
<?php

	function isRoot(){
		global $request;
		return $request == null;
	}

	/*
		$dir is the directory name - $list_dir is "true" if	you want to 
		list the directories or "false" if you want to list the files
	*/
	
	function scan($dir, $list = 'all'){
		static $hidden_files = array();
		static $hidden_dirs = array();
		$results = array();
		foreach(new DirectoryIterator($dir) as $fileInfo) {
    $fileName = $fileInfo->getFilename();
    if($fileInfo->isDot() || $fileName{0} === '.') continue;
    if($list == 'dir' && !$fileInfo->isDir()) continue;
    if($list == 'files' && $fileInfo->isDir()) continue;
    if(in_array($fileName, $hidden_files) && $fileInfo->isFile()) continue;
    if(in_array($fileName, $hidden_dirs) && $fileInfo->isDir()) continue;
    $results[] = array('inode' => $fileInfo->getInode(), 'filename' => pathinfo($fileName, PATHINFO_FILENAME), 'extension' => pathinfo($fileName, PATHINFO_EXTENSION), 'isDir' => $fileInfo->isDir());
		}
		return $results;
	}
	
	function isFavved($inode){
		global $favourites;
		return in_array($inode, $favourites);
	}
	
	function breadcrumbs(){
		global $request;
		$html = '';
		$composite_query = '';
		$queries = array_merge(explode('/', $request));
		$count = 0;
		foreach($queries as $query){
			$count++;
			$composite_query .= $query . '/';
			if($count < count($queries))	$html .= '<i class="icon-chevron-right"></i>';
			if($count < count($queries) - 1)
				$html .= '<a href="?' . $composite_query . '" title="' . $query . '">' . $query . '</a>';
			else
				$html .= '<a href="javascript:void(0)" title="' . $query . '" class="current default">' . $query . '</a>';
		}
		return $html;
	}
	
	function createNew(){
		global $request;
		return '<a href="javascript:createNew()" class="button new" path="' . getcwd() . '/' . $request . '"><i class="icon-plus"></i><span>Create new</span></a>';
	}
	
	function previous(){
		global $request, $hostname;
		$composite_query = $hostname;
		$queries = array_merge(explode('/', $request));
		for($i = 0; $i < count($queries) - 2; $i++){
			if($i == 0) $composite_query .= '?';
			$composite_query .= $queries[$i] . '/';
		}
		return '<a href="'. $composite_query .'" class="button prev"><i class="icon-arrow-left-thick"></i><span>Previous</span></a>';
	}
?>