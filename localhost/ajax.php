<?php
	$xml_file = 'settings.xml';
	$xml = simplexml_load_file($xml_file);
	
	if(isset($_GET['fav'])){
		$fav = $xml->favourites->addChild('fav');
		$fav->addAttribute('inode', $_GET['fav']);
		$fav->addAttribute('path', $_GET['path']);
		$fav->addAttribute('name', $_GET['name']);
		$fav->addAttribute('type', $_GET['type']);
		$xml->asXML($xml_file);
	}
	
	if(isset($_GET['unfav'])){
		$path = '//settings/favourites/fav[@inode="' . $_GET['unfav'] . '"]';
		$result = $xml->xpath($path);
		unset($result[0][0]);
		$xml->asXML($xml_file);
	}
	
	if(isset($_GET['create-folder'])){
		$path = $_GET['create-folder'] . $_GET['name'];
		if (!file_exists($path)) {
			echo mkdir($path, 0777, true);
			return;
		}
		echo 'A folder named <strong>' . $_GET['name'] . '</strong> already exists.';
		return false;
	}
	
	if(isset($_GET['create-file'])){
		$path = $_GET['create-file'] . $_GET['name'];
		if (!file_exists($path)) {
			echo 1;
			return fopen($path, 'c+');
		}
		echo 'A file named <strong>' . $_GET['name'] . '</strong> already exists.';
		return false;
	}
?>