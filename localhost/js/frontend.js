$(document).ready(function(){
	$('i.fav').each(function(){
		$(this).click(function(e){
			e.preventDefault();
			if($(this).hasClass('favved'))
				unfavouriteElement($(this).parent('a'));
			else
				favouriteElement($(this).parent('a'));
		});
	});
});

function createNew(){
	$('body').addClass('modalBlur');
	var modal_backdrop = document.createElement('div');
	var modal_wrapper = document.createElement('div');
	var modal = document.createElement('div');
	var title_wrapper = document.createElement('div');
	var close_link = document.createElement('a');
	var create_name = document.createElement('input');
	var create_folder = document.createElement('a');
	var create_file = document.createElement('a');
	var buttons_wrapper = document.createElement('div');
	$(modal_backdrop).attr('id','modal-backdrop');
	$(modal_wrapper).attr('id','modal-wrapper');
	$(modal).attr('id','modal');
	$(modal_wrapper).click(function(data, handler){ if (data.target == this) { closeModal(); } });
	$(close_link).click(closeModal);
	$(close_link).attr({ href: 'javascript:void(0)', title: 'Close' }).addClass('close').html('<i class="icon-close"></i>');
	$(create_name).attr({ id: 'create-name', type: 'text', name: 'create-name', placeholder: 'Name' });
	$(create_folder).addClass('button').attr({ href: 'javascript:void(0)', title: 'Create folder' }).addClass('folder').html('<i class="icon-folder-add"></i><span>Create folder</span>');
	$(create_file).addClass('button').attr({ href: 'javascript:void(0)', title: 'Create file' }).addClass('file').html('<i class="icon-document-add"></i><span>Create file</span>');
	$(create_folder).click(createFolder);
	$(create_file).click(createFile);
	$(buttons_wrapper).addClass('buttons-wrapper');
	$(buttons_wrapper).append(create_folder);
	$(buttons_wrapper).append(create_file);
	$(close_link).appendTo(title_wrapper);
	$('<h2>Create new...</h2>').appendTo(title_wrapper);
	$('<div class="clear">').appendTo(title_wrapper);
	$(title_wrapper).appendTo(modal);
	$(create_name).appendTo(modal);
	$('<span id="create-error">').appendTo(modal);
	$(buttons_wrapper).appendTo(modal);
	$(modal).appendTo(modal_wrapper);
	$(modal_wrapper).appendTo(modal_backdrop);
	$(modal_backdrop).hide().prependTo('body').fadeIn('fast');
}

function closeModal(){
	$('#modal-backdrop').fadeOut('fast', function(){
		$(this).remove();
		$('body').removeClass('modalBlur');
	});
}

function favouriteElement(link){
	$.get('localhost/ajax.php', { fav: $(link).attr('inode'), path: $(link).attr('path'), name: $(link).find('span').text(), type: $(link).attr('type') }).done(function(data) {
  location.reload();
	});
}

function unfavouriteElement(link){
	$.get('localhost/ajax.php', { unfav: $(link).attr('inode') }).done(function(data) {
  location.reload();
	});
}

function createFolder(){
	$('span#create-error').html();
	var name = $('input#create-name').val();
	if(name == '')
		$('span#create-error').html('You have to specify a name in order to create a folder.');
	else{
		$.get('localhost/ajax.php', { 'create-folder': $('a.new').attr('path'), name: name }).done(function(data) {
	  if(data == 1)
	  	location.reload();
	  else if(data == 0)
	  	$('span#create-error').html('Something went wrong, please try again.');
	  else
	  	$('span#create-error').html(data);
		});
	}
}

function createFile(){
	$('span#create-error').html();
	var name = $('input#create-name').val();
	var hasExtension = name.lastIndexOf('.') + 1;
	var extension = name.substr(hasExtension);
	if(name == '')
		$('span#create-error').html('You have to specify a name in order to create a file.');
	else if(!hasExtension || extension.length < 2)
		$('span#create-error').html('You have to specify a valid extension for the file.');
	else{
		$.get('localhost/ajax.php', { 'create-file': $('a.new').attr('path'), name: name }).done(function(data) {
	  if(data == 1)
	  	location.reload();
	  else
	  	$('span#create-error').html(data);
		});
	}
}

